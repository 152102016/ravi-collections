#include<stdio.h>
#include<omp.h>
int sum=0;

int main(){
  // before the parallelism... no. of threads=one

#pragma omp parallel if(1) //num_threads(5)
{

    printf("hello from thread: %d\n", omp_get_thread_num());

//#pragma omp master
  //           printf("master thread id: %d\n",omp_get_thread_num());   // always executed by thread 0 master


//#pragma omp atomic

  //    sum=sum+20;

#pragma omp single
              printf("single executed by thread: %d\n",omp_get_thread_num());

}
//after the parallelism... no. of threads=one

//printf("sum=%d\n",sum);

return 0;


}