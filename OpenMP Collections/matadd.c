#include<stdio.h>
#include<stdlib.h>
#include<sys/time.h>
#include<omp.h>

	int A[1024][1024],B[1024][1024],C[1024][1024];
double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

int main(){
	srand(3);

	for(int i=0;i<1024;i++){
			for(int j=0; j<1024;j++){
			  A[i][j]=rand()%100;
			  B[i][j]=rand()%100;
			}
	}
       double t1=rtclock(); 

      //#pragma omp parallel for //  --> outer loop

       //#pragma omp parallel for collapse(2)     // to parallelize both for loops , if add 'for' only it parallelizse only outer for loop
        
        
	    for(int i=0;i<1024;i++){

			
      #pragma omp parallel for // --> inner loop

            for(int j=0; j<1024;j++){
				C[i][j]=A[i][j]+B[i][j];
			//	C[j][i]=A[j][i]+B[j][i];
			}


	    }

        
       double t2=rtclock(); 
       printf("Time==%lf \n", (t2-t1)*1000);
}