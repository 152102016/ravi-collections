#include <stdio.h>
#include <omp.h>
int main()
{

    int n=5;   
    int a = 10;
    int b; 

    #pragma omp parallel for default(none) shared(n,a,b) 
    for(int i=0; i<n;i++)   
    {
        b = a + i; 
       
        printf ("b value is %d\n",b);
    }

    return 0;

}