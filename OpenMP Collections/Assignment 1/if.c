
#include <stdio.h>
#include <omp.h>

void test(int val)
{
    #pragma omp parallel if (val)
    if (val)
    {
        #pragma omp single    // must be executed by single available thread
        printf_s("val = %d, parallelized with thread %d\n",   
                 val, omp_get_thread_num());
    }
    else
    {
        printf_s("val = %d, serialized with thread %d\n", val, omp_get_thread_num()); 
    }
}

int main( )
{
    omp_set_num_threads(4);
    test(0);
    test(3);
}
