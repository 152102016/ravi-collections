/**
 * @author RookieHPC
 * @brief Original source code at https://www.rookiehpc.com/openmp/docs/copyin.php
 **/

#include <stdio.h>
#include <omp.h>
 
int a = 12345;
#pragma omp threadprivate(a)
 
/**
 * This program declares a global variable and specifies it as
 * threadprivate. This variable is then passed a copyin to the first parallel
 * region. In that region, the master thread modifies its value but other
 * threads will not see the update until the second parallel region; where the
 * variable will be passed as copyin again.
 **/
int main()
{

 
    #pragma omp parallel copyin(a)
    {
        #pragma omp master
        {
            printf("[First parallel region] Master thread changes the value of a to 67890.\n");
            a = 67890;
        }
 
        #pragma omp barrier           

        /**The barrier applies to the innermost enclosing parallel region, forcing every thread that belong to the team of that parallel region 
        to complete any pending explicit task. Only once all threads of that team satisfy this criterion will they be allowed 
        to continue their execution beyond the barrier.**/
 
        printf("[First parallel region] Thread %d: a = %d.\n", omp_get_thread_num(), a);
    }
 
    #pragma omp parallel copyin(a)
    {
        printf("[Second parallel region] Thread %d: a = %d.\n", omp_get_thread_num(), a);
    }
 
    return 0;
}
