#include<stdio.h>
#include<omp.h>

void main()
{
    int i,sum =0;
   
    omp_set_num_threads(4);

    #pragma omp parallel
    {
        #pragma omp for reduction(+:sum)
        for(i=1;i<=100;i++)
            sum += i;
    }
    printf("Sum = %d\n", sum);

}