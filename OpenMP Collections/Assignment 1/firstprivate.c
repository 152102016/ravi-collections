#include<stdio.h>
#include<omp.h>

int main()
{
    int i; //x;
    //x = 44;
    omp_set_num_threads(3);  // set the number of threads as 3

#pragma omp parallel for firstprivate(x)   // lastprivate(list) clause actually copy the value of x 
//                                          from the last executed thread...
    for (i=0; i<10; i++){

        int x = i;
        printf("Thread number: %d    x: %d\n", omp_get_thread_num(), x);
    }

    printf("x is %d\n", x);

return 0;

}