// Data Structures

//Tuples (Very common in sparks !!)
// Tuples is immutable list, collection of same or different types of object but they could be thought as columns
// This will useful for passing the entire rows of data

val captainStuff = ("Picard", "Enterprise-D", "NCC-1701-0")  // collection of all the strings that mention here

println(captainStuff)

//Imp point -> refer to Individual fields with ONE-BASED index  // many language you use  0 based index

println(captainStuff._3)

// Can create Key value pair using "->"

val picardShip = "Picard" -> "Enterprise-D"
println(picardShip._2)  // access the fields
println(picardShip("Picard"))


//imp thing also that "You can mix different types as well"
val bunchOfStuff = ("Kirk", 1964, true)


//Lists
// Difference between Tuples and Lists here is that "Lists cannot hold different types of items as Tuples can do..."

val shipList = List("Enterprise", "Defiant", "Deep Space Nie", "Voyagers")

// Main point is that "access individual fields with Zero-Based index"

println(shipList(0))
println(shipList(1))

println(shipList.head)  // head will return the first field which is at 0Th index

//***imp*****
println(shipList.tail)  // return the list of all the elements excepts 'head'

//Iterate through the list

for(ship <- shipList) {println(ship)}

// apply function literals into your Lists

val backwardShip = shipList.map((ship: String) => {ship.reverse})  // reversing the Lists
// map is given all the inputs in the Lists take as Input and get output whatever you want

println(backwardShip)  // print all the Strings of Lists in reverse order...

// "reduce" function combines all the items in the collections

val numberList = List (1, 2, 3, 4, 5, 6, 7, 8)
val sum = numberList.reduce((x: Int, y: Int) => x + y)

// reduce() takes one value from previous iteration and current iteration value and it will do what operation you define here
// then return one value that assign here in "x" and again take x as previous iteration value and y as current iteration value again do computation

// Filter function used to clean your data, use for clean outliers present in your dataset
val iHateTwos = numberList.filter((x: Int) => x!=2)   // get all Lists elements except 2, 2 will be filtered out

val iHateFives = numberList.filter(_ != 5)  // whatever comes in if not equal to 5, i just filter it out!!
// this above kind of function literal declare as very short hand called wildcard Syntax


// ***** This 'map', 'reduce' and 'filter' can be used or paralleled heavily
// *** sparks has its own map,reduce and filter also and they works in same way but in parallel manner  -> as Mapreduce topic


// Concatenating Lists
val moreNumbers = List (8, 9, 10, 11)

val lotsOfNumbers = numberList ++ moreNumbers    // concatenate two lists

// Reverse Lists

val reversed = numberList.reverse  // that gives the list of some reverse order

// Sort the Lists
val sorted = reversed.sorted

// Filter out distinct values -> from duplicate values present in Lists
val lotsOfDuplicates = numberList ++ numberList
val distinctValues = lotsOfDuplicates.distinct

// search for items inside a lists
val hasFive = iHateFives.contains(5)


// Maps  -> useful for Key-value lookups on distinct keys essentially and you can think it off like dictionaries in other languages

val shipMap = Map("kirk" -> "Enterprise", "Picard" -> "Enterprise-D", "Sisko" -> "Deep space Nine", "janway" -> "Voyager")

//access
println(shipMap("janway"))  // get values, search by key

// println(shipMap("archer"))  // exception comes because key not found

println(shipMap.contains("Archers"))  // returning false


//Exception handling
val archerShip = util.Try(shipMap("archer")) getOrElse("Unknown value")   // works as try and catch block

println(archerShip)

// That's how you search for data and handle the missing data...

