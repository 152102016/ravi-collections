// Two types of Variables in Scala -> VALUES and VARIABLES

// VALUES -> are immutables (not change once define)
// SYNTAX of VALUES -> val _name_:_space_datatype(string) = "assign the value"

val hello: String = "Hii!" // to evaluate this expression, "hit at first green button or use Ctrl + W"

println(hello)

// VARIABLES are mutable (change the value of variables later on after the defining once)

var helloThere: String = hello
helloThere = hello + " There!"   // change the variable value

println(helloThere)


// These above two basic feature supports in Functional programming
// and this kind of centred around idea of passing functions and potenially running them in parallel...
// that' why it has good idea or match for apache spark... where you do lots of distributed processing.

// We are mostly use this immutable constant with val keyword... that is becuase to avoid bunch of thread safety issues.
// Ex-> when we are passing in functions and executed by different threads so, if we are not define by immutable. some thread
// might change the value of this 'val'... which gives result will become undefined.
// TO avoid this race conditions, it's better to stick to immutable constants whenever is possible...


// With immutable variables you can change other immutable variables and define it...
val immutableHelloThere = hello + " There!" // atomic operation
println(immutableHelloThere)


// Other dataTypes -> Int, Booleans, Char, Double precision, Single Precision, Long, Byte
val numberOne: Int = 1
val truth: Boolean = true // (true/false is lower case only)
val letterA: Char = 'a'
val pi: Double = 3.14159265 //Double Precision
val piSinglePrecision: Float = 3.14159265f //Single Precision -> 'f' at the end
val largeNumber: Long = 1234567890
val smallNumber: Byte = 127


// printf Style -> fString and sString

println(f"Pi is about $piSinglePrecision")  // "fString" -> easy to concatenate

// padding for your Double and Float type
println(f"Pi is about $piSinglePrecision%.3f") // 3 digits after decimal point

// padding for your integer type

println(f"Zero padding on the left: $numberOne%05d") // 05d -> assign "0" 5 places on the left, d is for integer

// using 's Style' -> substitute all the different datatypes in one string
println(s"The s prefix is used like $numberOne $truth $letterA")

// substitute any value with the regular expressions or any expressions
println(s"The  s prefix is not limited to variables; I can include any expression. like ${20+30} ")

// using regular expressions -> extract some times like log files, we use lot of regular expressions, -> Sir provide one book to learn more about this

val theUltimateAnswer: String = "To life, the universe, and everything is 42"

// Now i define one pattern which will extract this number 42 from there...
val pattern = """.* ([\d]+).*""".r
// .* -> starting with anything
// space -> space followed by space
// () -> inside parenthesis here that will evaluated as your extraction
// [\d]+ -> extracting any number, + means number of numbers
// .* -> means followed by anything

// Now this regular expression i apply onto this string and extract that

val pattern(answerStirng) = theUltimateAnswer // basically i want to extract something on theultimateAnswer
// ([\d]+) -> what i will extract define in
// (answerString) -> where it will be extract

// and this answerString -> gives a String and convert to any dataType
val answer = answerStirng.toInt

println(answer)

// Dealing with Booleans

val isGreater = 1 > 2
val isLesser = 1 < 2

// Dealing with logical operators
val ispossible = isGreater & isLesser  // & -> bitwise operator
// another way
val anotherWay = isGreater && isLesser // && -> logical operator (AND)

// But Mostly in all the cases evaluating any boolean value use logical operator

val anotherWay = isGreater || isLesser

// Comparisions
val picard: String = "picard"
val bestCaptain: String = "picard"

val isBest: Boolean = picard == bestCaptain
