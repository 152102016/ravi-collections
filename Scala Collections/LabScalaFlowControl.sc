// if -else
if(1>3) println("No way") else println("All is well")

//Matching - Similar to 'switch' in other languages
val number = 4
number match{
  case 1 => println("One")
  case 2 => println("Two")
  case 3 => println("Three")
  case _ => println("Something else!")
}

// For loops
for(x <- 1 to 4){
  val squared = x * x
  println(squared)
}

//While loop  -> not used much in Scala language
var x = 10
while(x>=0)
  {
    println(x)
    x-=1
  }

//Expressions
//"Returns" the final value in the block automatically

// Expression define inside {}
{val x = 10; x+50 } // with ;(semicolon) write multiple expressions in one line
//returning the last block value automatically

// That is the structure of Functional Programming where expressions are implicit and they will return last value
// which is related!! and do automatically; if you want explicitly written value that also you can do here!



