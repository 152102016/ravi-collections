//Functions

// Format is "def <function name> (parameter name: type...any number of parameters) : return type = {expression}"

def squareIt (x: Int) : Int = {
  x*x         // {expression} return last block evaluate automaticaaly
}

def cubeIt (x: Int) : Int = {x * x * x}

println(squareIt(3))
println(cubeIt(10))


//Functions can take other functions as parameter

// def <function name> (parameter name : type, <pass another function (f) : which is taking integer type as Input (Int)> )
// (=>) and given Integer type as return or output (Int) and : (for 1st function returning as Integer type (Int) ) = {expression can take the function and evaluate as x (f(x))}

def transformInt (x: Int, f: Int => Int) : Int = {
  f(x)
}
// This will use lots inside Spark programming so, look at once and solve and understand carefully


val result = transformInt(2, cubeIt)

println(result)


val resultOther = transformInt(4, squareIt)


// "Lambda function"/ "anonymous function" / "function literals"
transformInt(3, x => x * x * x)  // you can also pass guard of the function directly (expression of the function directly)
// This is inline function that directly define here...

// ***This will also use a lot inside Sparks***

transformInt(10, x => x/2)


transformInt(3, x => {val y = x * 4; y * y}) // use complex expressions also
// {val y = x * 4; y * y} expression evaluated and return as Int... f: Int => evaluate expression Int











