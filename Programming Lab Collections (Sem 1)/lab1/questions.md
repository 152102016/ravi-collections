# Assignment 1 (deadline: 26-Aug 2021)

## Instructions

  1. This `questions.md` files is a Markdown files.
     Markdown is a simple markup
     language that you can use to quickly format text documents.
     The basic syntax of Markdown to achieve headings, subheadings,
     bullets, numbered lists, paragraphs, and so on can be found in
     this link: https://www.markdownguide.org/basic-syntax/

  2. Type your answers (using Markdown syntax where applicable)
     against the line starting with "Ans.". Replace existing text
     when you type your answers.

  3. When you have answered all the questions given in this file,
     commit your changes, and push it back to your bitbucket repo.
     

## Questions

1. What is your Roll number?

Ans. 152102016

2. What is your name?

Ans. RAVI KUMAR CHOUBEY

3. Paste the last 20 lines of your `syslog` file.

Ans.
   Aug 26 21:10:27 ravi systemd[1]: Started Fingerprint Authentication Daemon.
   Aug 26 21:10:33 ravi NetworkManager[600]: <info>  [1629992433.6427] agent-manager: agent[7bf816241ffcf4e6,:1.79/org.gnome.Shell.NetworkAgent/1000]: agent registered
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 31 with keysym 31 (keycode a).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 32 with keysym 32 (keycode b).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 33 with keysym 33 (keycode c).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 34 with keysym 34 (keycode d).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 35 with keysym 35 (keycode e).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 38 with keysym 38 (keycode 11).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 39 with keysym 39 (keycode 12).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 36 with keysym 36 (keycode f).
   Aug 26 21:10:34 ravi gnome-shell[1664]: Window manager warning: Overwriting existing binding of keysym 37 with keysym 37 (keycode 10).
   Aug 26 21:10:43 ravi gnome-shell[1664]: JS ERROR: TypeError: area is null#012padArea@resource:///org/gnome/shell/ui/workspace.js:1101:9#012_updateWindowPositions@resource:///org/gnome/shell/ui/workspace.js:1334:20#012_realRecalculateWindowPositions@resource:///org/gnome/shell/ui/workspace.js:1311:14#012_recalculateWindowPositions/this._positionWindowsId<@resource:///org/gnome/shell/ui/workspace.js:1286:18
   Aug 26 21:10:45 ravi systemd[1353]: Started Application launched by gnome-shell.
   Aug 26 21:10:46 ravi dbus-daemon[1363]: [session uid=1000 pid=1363] Activating via systemd: service name='org.gnome.Terminal' unit='gnome-terminal-server.service' requested by ':1.122' (uid=1000 pid=2833 comm="/usr/bin/gnome-terminal.real " label="unconfined")
   Aug 26 21:10:46 ravi systemd[1353]: Starting GNOME Terminal Server...
   Aug 26 21:10:46 ravi dbus-daemon[1363]: [session uid=1000 pid=1363] Successfully activated service 'org.gnome.Terminal'
   Aug 26 21:10:46 ravi systemd[1353]: Started GNOME Terminal Server.
   Aug 26 21:10:47 ravi systemd[1353]: Started VTE child process 2845 launched by gnome-terminal-server process 2836.
   Aug 26 21:10:47 ravi systemd[1353]: gnome-launched-org.gnome.Terminal.desktop-2831.scope: Succeeded.
   Aug 26 21:10:58 ravi systemd[1]: fprintd.service: Succeeded.


4. Where is your "HOME" directory located in the system?

Ans. 
   ravi@ravi:~$ pwd
   /home/ravi
   ravi@ravi:~$ cd ..
   ravi@ravi:/home$ pwd
   /home

5. Paste the output of the command `ls -1a ~`.
   (The option passed is (one)-a and not `-la`)

Ans.
   .
   ..
   152102016
   .bash_history
   .bash_logout
   .bashrc
   .cache
   .config
   Desktop
   Documents
   Downloads
   .gitconfig
   .gnupg
   lab1
   .local
   .mozilla
   Music
   Pictures
   .profile
   Public
   .ssh
   .sudo_as_admin_successful
   Templates
   .vboxclient-clipboard.pid
   .vboxclient-display-svga-x11.pid
   .vboxclient-draganddrop.pid
   .vboxclient-seamless.pid
   Videos


6. Write a 500 word summary (in your own words) of the Linux Filesystem.

Ans.    
   Linux Filesystem follows a hierarchical structure of tree and starting node is 'root'. This structure follows a standard layout recommended by Filesystem Hierarchy Standard (FHS) which is maintained by the Linux Foundation. All Nodes are consist of directories, sub-directories, and data files. This Filesystem is also had the same behaviour as a Unix-like system. These systems keep things simple and treat everything as a sequence of bytes and this sequence of bytes are known as Files to the OS. So, in that sense, we say that everything in Linux is a File even your hardware devices, printer, disk, main-memory etc. all are Files. Unlike Windows which has multiple roots, Linux has only one root. The root directory has all other directories and files of the system that reside and is represented by a forward slash (/). 
   Here is some of my Linux system hierarchy -> 'root -> home -> ravi -> Desktop'.... remember all words are Case Sensitive which means Desktop and desktop is different. Now let's goes some more details inside that take some specific things .. ex -> 1. /home directory -> home directory is where the user can store its files and documents like images, videos, etc. Linux is multi-user accessibility means the system administrator permits to access specific users to a specific directory only. The home directory contains your personal configuration files called as dot files and these files are usually hidden, so you need one command to view them that is 'ls -a'. if some conflicts will come between your files and configuration files so, setting in the personal files has priority.
   2. /bin and /sbin directories -> bin is a short form of binary where Linux can keep their basic programs and applications. Binary files are the executable files that contain the source code of almost all basic Linux commands that can be present here like ls, pwd, cat, rm, echo, etc. These source codes have different functionality like 'ls' used to list contents of a directory, pwd -> present working directory, cat has two functionality - one is to concatenate two files or more files and the second one is it can be used to view one file, rm is used to remove directory/files command,... 
   Now come to see some shin on 'sbin' -> sbin is a short form of system binary, it also stores executable programs but these executable programs are system configuration files and administrator tasks. In other words, these directories contain some essential files for booting, restoring, and recovering. For example, fsck -> a file system check and repair utility, reboot -> restart the system. So, to execute these commands you need administrator privilege which is symbolized in Linux using "sudo" -> superuser privileges... for example -> for reboot using sudo reboot. 
   3. /var directory -> var full form is variables, that contains variable data like system log files, mail, and printer spool directories, temporary files, etc. we take examples like use command -> /var/log gives all log files for your system and its applications, use a command like -> /var/crash -> information about every time process crashed.

7. Create a report that includes the screenshots of how you
   setup git and keyless SSH on your machine, and the commands
   you used to clone, pull, commit and push the submission to
   your repository.
   
Ans. (Create a pdf file with filename <YOUR_ROLLNO>.pdf. Add this
      this file to your repository and push it to bitbucket)


