#include "Matrix.h"
#include <iostream> 
#include "log.h" 
#include<string.h>  
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define N 100



void Matrix :: matrix_addition(){                          // matrix addition

if (nor == nor1 && noc == noc1){                                       // check no. of rows and columns are equal or not for matrix addition...

for (i=0; i<nor; i++)
        {
            for ( j=0; j<noc; j++)
            {
                matrix_add[i][j] = matrix1[i][j] + matrix2[i][j];
                cout << matrix_add[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        
        L_(linfo) << "Matrix addition successful"<<endl; 
}

else
{
L_(lerror) << "matrix-addition is not possible"<<endl;

}

}


void Matrix :: matrix_subtraction(){                             // matrix subtraction

if (nor == nor1 && noc == noc1){                        // check no. of rows and columns are equal or not for matrix subtaction...

for (i=0; i<nor; i++)
        {
            for ( j=0; j<noc; j++)
            {
                matrix_sub[i][j] = matrix1[i][j] - matrix2[i][j];
                cout << matrix_sub[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        
        L_(linfo) << "Matrix subtraction successful"<<endl; 
}

else
{
L_(lerror) << "matrix-subtraction is not possible"<<endl;


}

}

void Matrix :: matrix_multiplication(){                      // matrix multiplication

if (noc == nor1){                               // check matrix multiplication condition

for (int i = 0; i < nor; i++) {
        for (int j = 0; j < noc; j++) {
            matrix_mul[i][j] = 0;
 
            for (int k = 0; k < nor1; k++) {
                matrix_mul[i][j] += matrix1[i][k] * matrix2[k][j];
            }
 
            cout << matrix_mul[i][j] << " ";
        }
 
        cout << endl;
    }
cout << endl;

L_(linfo) << "Matrix multiplication successful"<<endl; 
}

else
{
L_(lerror) << "matrix-multiplication is not possible"<<endl;


}

}


void Matrix :: matrix_transpose_x1(){                       // transpose of a matrix

for (i=0; i<nor; i++)                
        {
            for ( j=0; j<noc; j++)
            {
                matrix_trans_x1[i][j] = matrix1[j][i];
                cout << matrix_trans_x1[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        
        L_(linfo) << "Transpose of a matrix successful"<<endl; 
}


void Matrix :: determinant_matrix(){                    // compute determinant .........

if (nor == noc ){

for (int i=0; i<nor; i++){
for(int j = 0; j<noc; j++){

matrix1[i][j];
}}

cout << determinantOfMatrix(matrix1, nor) << endl;
L_(linfo) << "determinant computation is successful"<<endl; 

}

else 
{
L_(lerror) << "determinant is not possible"<<endl;
}

}



void Matrix :: subMatrix(int mat[N][N], int temp[N][N], int p, int q, int n) {
   int i = 0, j = 0;
   // filling the sub matrix
   for (int row = 0; row < n; row++) {
      for (int col = 0; col < n; col++) {
         // skipping if the current row or column is not equal to the current
         // element row and column
         if (row != p && col != q) {
            temp[i][j++] = mat[row][col];
            if (j == n - 1) {
               j = 0;
               i++;
            }
         }
      }
   }
}
int Matrix :: determinantOfMatrix(int matrix[N][N], int n) {
   int determinant = 0;
   if (n == 1) {
      return matrix[0][0];
   }
   if (n == 2) {
      return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
   }
   int temp[N][N], sign = 1;
   for (int i = 0; i < n; i++) {
      subMatrix(matrix, temp, 0, i, n);
      determinant += sign * matrix[0][i] * determinantOfMatrix(temp, n - 1);
      sign = -sign;
   }
   return determinant;


}

