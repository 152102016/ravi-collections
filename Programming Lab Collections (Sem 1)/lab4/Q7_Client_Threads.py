import socket
from threading import Thread
import random
import time

GET_IP = socket.gethostbyname(socket.gethostname())  # get the IP address of machine

PORT = 9999  # port number

ADDRESS = (GET_IP, PORT)

DISCONNECT_MESSAGE = "!DISCONNECT"


class Real_time_Threads(Thread):  # class created for threads

    def __init__(self, name):
        Thread.__init__(self)
        self.name = name


    def run(self):
        c = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # family, stream  specify ipv4 or ipv6, if nothing specify
    # use-> ()
        c.connect(ADDRESS)

        counter = 0

        while counter < 1:
            client_delay = random.randint(1, 10)  # wait random amount of time

            time.sleep(client_delay)

            print(f"Time {client_delay}s: {self.name} sent message to server\n")

            data = f"Hello from {self.name}\n" # thread name sending to server
            c.send(data.encode("utf-8"))  # sent message to server from client



            data_recent_receive = c.recv(4096).decode("utf-8")  # message received from server
            print(f"{data_recent_receive}")

            data_receive = c.recv(4096).decode("utf-8")  # message received from server
            print(f"{data_receive}")

            counter += 1


# Creating new threads
client_thread1 = Real_time_Threads("Thread 1")
client_thread2 = Real_time_Threads("Thread 2")
client_thread3 = Real_time_Threads("Thread 3")
client_thread4 = Real_time_Threads("Thread 4")
client_thread5 = Real_time_Threads("Thread 5")
client_thread6 = Real_time_Threads("Thread 6")
client_thread7 = Real_time_Threads("Thread 7")
client_thread8 = Real_time_Threads("Thread 8")
client_thread9 = Real_time_Threads("Thread 9")
client_thread10 = Real_time_Threads("Thread 10")

# Starting new Threads
client_thread1.start()
client_thread2.start()
client_thread3.start()
client_thread4.start()
client_thread5.start()
client_thread6.start()
client_thread7.start()
client_thread8.start()
client_thread9.start()
client_thread10.start()
