from threading import *
import time
import random

# it is same as something producer consumer problem

e = Event()  # create Event Object...

producer_time1 = random.randint(1, 5)

mutex = 1  # declare global variable which notify how many are Events occurred...


def consumer():
    while True:
        # print("Consumer thread waiting for updating...")
        consumer_time = random.randint(1, 5)
        consumer_time1 = random.randint(1, 5)

        e.wait()

        print(f'Time {consumer_time + consumer_time1 + producer_time1}s : Event processed ...')

        if mutex == 10:
            consumer_time = random.randint(1, 5)
            consumer_time1 = random.randint(1, 5)

            e.wait()  # wait method ... wait for consumer do not processed Events unless set gives you green signal
            # or set is 1

            print(f'Time {consumer_time + consumer_time1 + producer_time1}s : Event processed ...')
            # Event Consumed....

            print("All events have been processed...")


def producer():
    global mutex

    while mutex <= 10:  # Max Events occurred...
        producer_time = random.randint(1, 5)  # used to given random item event occurred...

        time.sleep(producer_time)  # that consumer wait so, always give chance to consume it

        print(f"Time {producer_time}s : Event scheduled at {producer_time + producer_time1}s ")  # notify when event
        # is scheduled...

        print(f'Time {producer_time + producer_time1}s : Event occurred...')  # Event occurred by producer...

        e.set()  # if set method gives green signal then 'wait' will be Open to processed Events...

        e.clear()  # Clear method used to stop the Event to be consumed and then set will be stop or 0

        mutex = mutex + 1


EG = Thread(target=producer)  # creating Threads...

EC = Thread(target=consumer)

EG.start()
EC.start()
