import pickle   # it is non secure library but easy to use for receive and send
import socket
from statistics import mean

# dir(socket)

try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # family, stream  specify ipv4 or ipv6, if nothing specify
    # use-> ()

    print("Server end point created successfully")

except Exception as e:
    print("socket failed", e)

# bind(())  tuple : ip,port
# port = int(input("Enter port number"))  # port>1023   port number of server

# bind
s.bind(('localhost', 9999))  # '' --> any client can be bind by server, if you are specify the ip address that only
# client  with server

# listens
s.listen(4)  # listen(4)  --> buffer size is 4 means max 4 client connected at a time...
print("server is listening........")

# create an array of 10 tuples..


while True:  # client sends data many times

    c_socinfo, addr = s.accept()  # accept method return two things socinfo and address

    print("Got connection from", addr)

    # sleep(1)

    data = c_socinfo.recv(4096)  # data store into the buffer of size 1024  and then store in any variable
    # print(d ata)

    data_variable = pickle.loads(data)    # taken value from server in string format also using pickle...

    receive_list = [item for t in data_variable for item in t]   # convert tuple of list into normal list...

    # print(data_variable)
    # print(receive_list)

    # print(receive_list)

    # 'data'
    # print(list(data))  # data is in bytes form convert it into list...

    odd_i = []  # create two list to taken values from odd indices in one list and even in other...
    even_i = []
    for i in range(len(receive_list)):  # check condition odd and even
        if i % 2 == 0:
            even_i.append(receive_list[i])
        else:
            odd_i.append(receive_list[i])


    def Operations(even_i, odd_i):  # do mean operation here
        mean1 = mean(even_i)
        mean2 = mean(odd_i)

        return mean1, mean2


    average = Operations(even_i, odd_i)


    def Operations1(even_i, odd_i):  # do maximum operation here
        max1 = max(even_i)
        max2 = max(odd_i)

        return max1, max2


    maximum = Operations1(even_i, odd_i)


    def Operations2(even_i, odd_i):  # do minimum operation here
        min1 = min(even_i)
        min2 = min(odd_i)

        return min1, min2


    minimum = Operations2(even_i, odd_i)

    send_data = [average, maximum, minimum]  # append values in a list...

    # print(send_data)

    data_string = pickle.dumps(send_data)    # pickle is used to pass the string
    c_socinfo.send(data_string)

    # c_socinfo.send(bytes(send_data))  # use bytes to send the list to client

    c_socinfo.close()
