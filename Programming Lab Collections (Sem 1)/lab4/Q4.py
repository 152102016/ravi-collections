import random
import time
from threading import Barrier, Thread

time1 = random.randint(1, 20)  # used to generate random amount of time
time2 = random.randint(1, 20)
time3 = random.randint(1, 20)
time4 = random.randint(1, 20)
time5 = random.randint(1, 20)

time6 = random.randint(0, 9)
time7 = random.randint(0, 9)
time8 = random.randint(0, 9)
time9 = random.randint(0, 9)
time10 = random.randint(0, 9)

barrier = Barrier(5)  # create Barrier Object


def wait_on_barrier(name, time_to_sleep, time_to_sleep1):  # wait_on_barrier function contain barrier method...

    time.sleep(time_to_sleep)  # reaches a mall at random amount of time, we don't know which Person will come first...

    print(f"Time {time_to_sleep}s: {name} reached the mall\n")  # print time and name using fString

    barrier.wait()  # wait for others and when all Persons will come, we enter them in mall at the same time

    # timeout is used to wait in barrier of fixed amount of time

    print(f"Time 20s : {name} enters the mall\n")
    # used to display all will come or not

    if time.sleep(time_to_sleep1) == 0:  # spend time in mall and all persons will leave some random amount of time...
        print(f"Time 3{time_to_sleep1}s: {name} leaves the mall\n")

    else:
        print(f"Time 2{time_to_sleep1}s: {name} leaves the mall\n")


Person1 = Thread(target=wait_on_barrier, args=["Person 1", time1, time6])  # create Persons as Threads contain
# 'name', 'time to wait for others' and 'leave the mall at different time'...

Person2 = Thread(target=wait_on_barrier, args=["Person 2", time2, time7])
Person3 = Thread(target=wait_on_barrier, args=["Person 3", time3, time8])
Person4 = Thread(target=wait_on_barrier, args=["Person 4", time4, time9])
Person5 = Thread(target=wait_on_barrier, args=["Person 5", time5, time10])

Person1.start()
Person2.start()
Person3.start()
Person4.start()
Person5.start()
