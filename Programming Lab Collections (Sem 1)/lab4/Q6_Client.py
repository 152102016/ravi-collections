import pickle   # it is non secure library but easy to use for receive and send
import socket

try:
    c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("Client socket endpoint created")

except Exception as e:
    print("socket failed", e)

# connect   --> server is listen the request, client is trying to connected to the server...
# when server accepts the request, again effectively sends the data between the client and server...

# connect(('ip', port))  connect method contain tuple as a parameter...

#


c.connect(('localhost', 9999))  # 127.0.0.1 --> local host, we use same machine as host and server
# port number > 1023   enter server's port number -> enter that server port number which you want to be connected...

A = [(2, 5), (5, 3), (4, 7), (3, 8), (3.444, 5.334), (2.77, 7), (1, 6), (4, 9), (1, 3), (5, 7)]

print(f"Array A :- {A}")

data_string = pickle.dumps(A)    # pickle is used to pass the string
c.send(data_string)

'''
print(f"Array A :- {A}")

for values in A:
    c.send(bytes(values))  # if you want to send data to the server...'''

# c.send(bytes("\nClient is connected to the server", 'utf-8'))  # if you want to send data to the server...

data = c.recv(4096)
data_variable = pickle.loads(data)   # taken value from server in string format also using pickle...

print(f"Array B :- {data_variable}")

'''
sleep(1)


data = c.recv(4096)  # receive from server and have buffer size is 4096...

L = list(data)  # data comes from server, first convert it into list because data comes is in the form of bytes...

it = iter(L)  # create a iterator
print(f"Array B :- {list(zip(it, it))}")  # zip is used to group items at a time here, i am grouping two items at a
# time...

'''

c.close()
