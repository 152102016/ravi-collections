import random
import time
from threading import Semaphore, Thread

time1 = random.randint(1, 5)  # used to generate random amount of time
time2 = random.randint(1, 5)
time3 = random.randint(1, 5)
time4 = random.randint(1, 5)
time5 = random.randint(1, 5)

time6 = random.randint(5, 10)
time7 = random.randint(5, 10)
time8 = random.randint(5, 10)
time9 = random.randint(5, 10)
time10 = random.randint(5, 10)

semaphore = Semaphore(2)  # create Barrier Object


def Semaphore(name, time_to_reach,
              time_to_spend):  # Semaphore function contain 'name','time to reach' and 'time to spend' as a parameter
    time.sleep(time_to_reach)

    print(f"Time {time_to_reach}s: {name} reached the shop\n")  # print time and name using fString

    semaphore.acquire()  # acquire method ensure that only two person can go inside at a time and value of semaphore
    # become decrement till max value 2 achieved, after that no person allowed wait at reached points till anyone
    # left the shop

    print(f"Time {time_to_reach}s : {name} entered the shop\n")  # print time and name using fString

    time.sleep(time_to_spend)  # random time spend in shop

    semaphore.release()  # left the shop when time spend is over and semaphore value is incremented...

    print(f"Time {time_to_spend}s: {name} left the shop\n")


Person1 = Thread(target=Semaphore, args=["Person 1", time1, time6])  # create Persons as Threads contain
# 'name', 'time to reach the shop' and 'time to spend in shop'...

Person2 = Thread(target=Semaphore, args=["Person 2", time2, time7])
Person3 = Thread(target=Semaphore, args=["Person 3", time3, time8])
Person4 = Thread(target=Semaphore, args=["Person 4", time4, time9])
Person5 = Thread(target=Semaphore, args=["Person 5", time5, time10])

Person1.start()
Person2.start()
Person3.start()
Person4.start()
Person5.start()
