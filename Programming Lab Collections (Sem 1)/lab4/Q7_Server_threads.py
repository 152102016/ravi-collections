import socket
from threading import Thread
import random
import time

GET_IP = socket.gethostbyname(socket.gethostname())  # get the IP address of machine

PORT = 9999  # port number

ADDRESS = (GET_IP, PORT)

DISCONNECT_MESSAGE = "!DISCONNECT"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # family, stream  specify ipv4 or ipv6, if nothing specify
    # use-> ()

s.bind(ADDRESS) # '' --> any client can be bind by server, if you are specify the ip address that only
# client  with server


def client_handle(c_socinfo, addr):  # handle threads one by one...
    print(f"Connected to :{addr} ")
    connected = True
    while connected:
        data = c_socinfo.recv(4096).decode("utf-8")  # receive message from threads

        server_wait = random.randint(1, 5)  # wait random amount of time
        time.sleep(server_wait)

        send_recent_data = f"Msg sent : {data}"
        c_socinfo.send(send_recent_data.encode("utf-8"))  # message recently reach send immediately

        time.sleep(server_wait)

        send_data = f" Msg received : {data}"
        c_socinfo.send(send_data.encode("utf-8"))  # message send to individual threads one by one...

        if data == DISCONNECT_MESSAGE:
            connected = False


    c_socinfo.close()


def start():
    print("Server is starting...")
    s.listen()   # listen the client
    print(f"Server is listening on {GET_IP}:{PORT}")

    while True:
        c_socinfo, addr = s.accept()
        thread = Thread(target=client_handle, args=(c_socinfo, addr))
        thread.start()


print("[STARTING] server is starting...")
start()
