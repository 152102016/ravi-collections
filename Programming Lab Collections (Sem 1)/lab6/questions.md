# Lab 6 (deadline: 26-Oct 2021 23:59 hrs)

## Instructions

  1. The instructions on how to submit each answer is indicated below the
     question (in the line starting with "Ans."). Please follow those
     instructions carefully.

  2. Use code-blocks when you are pasting outputs seen on the terminal.


## Questions    

1. **[1 mark]** With an example, explain the output of the `diff` command.

**Ans.**. 
```
	The diff command used to compare the difference in two files(analyse the difference between two lines in two files). After analysing the two files which makes you to change the text in the first file to be identical to that of the text in the second file.
	=> The <,> sign refer to the first and second files respectively. 
	
	=> Special Symbols used in diff command :-
      	a -> add, d -> delete, c -> change

      	Ex: -
      	(3,6c3,6) indicates the line 3 to 6 of the first file must be changed (c) to match the lines 3 to 6 of the second file.
      	(2a3,4)  indicates after line 2 of first file add line 3 and 4 of second file.
      	(3,4d2)  indicates delete 3rd and 4th from first file.

    	ravi@DESKTOP-6VFFD7I:~$ ls
    	152102016  b.csv        csv.rollnos.sorted  diffab   gem5       hello.out            microbench           scs
    	a.csv      b.txt        csv.rollnos.unique  diffba   hello      iecho                my_pipe
    	a.txt      cat_cut      diffa1a             diffba1  hello.c    iecho.c              myscript.sh
    	a1.csv     csv.merged   diffa1b             fac      hello.err  lab6_soft_hard_link  myscript2.sh
    	a1.txt     csv.rollnos  diffaa1             fac.c    hello.in   ls.in                riscv-gnu-toolchain
    
    	ravi@DESKTOP-6VFFD7I:~$ cat a.txt
    	My name is Ravi kumar choubey
    	Mtech socd
    	ravi@DESKTOP-6VFFD7I:~$ cat a1.txt
    	My name is Ravi kumar choubey
    	Mtech socd
    	ravi@DESKTOP-6VFFD7I:~$ cat b.txt
    	My name is Ravi kumar choubey
    	Mtech socd
    	152102016
    	152102016@smail.iitpkd.ac.in

    	ravi@DESKTOP-6VFFD7I:~$ diff a.txt b.txt > diffab     --> compare first with third and write in diffab
    	ravi@DESKTOP-6VFFD7I:~$ cat diffab
    	2a3,4
    	> 152102016
    	> 152102016@smail.iitpkd.ac.in
    
	ravi@DESKTOP-6VFFD7I:~$ diff b.txt a.txt > diffba    ---> compare third with first and write in diffba
	ravi@DESKTOP-6VFFD7I:~$ cat diffba
	3,4d2
    	< 152102016
    	< 152102016@smail.iitpkd.ac.in
    
    	ravi@DESKTOP-6VFFD7I:~$ diff a.txt a1.txt > diffaa1   ---> compare first with second and write in diffaa1
    	ravi@DESKTOP-6VFFD7I:~$ cat diffaa1
    	ravi@DESKTOP-6VFFD7I:~$
    
    	ravi@DESKTOP-6VFFD7I:~$ diff a1.txt a.txt > diffa1a    ---> compare second with first and write in diffa1a
    	ravi@DESKTOP-6VFFD7I:~$ cat diffa1a
    	ravi@DESKTOP-6VFFD7I:~$
    
    	ravi@DESKTOP-6VFFD7I:~$ diff a1.txt b.txt > diffa1b    ---> compare second with third and write in diffa1b
    	ravi@DESKTOP-6VFFD7I:~$ cat diffa1b
    	2a3,4
    	> 152102016
    	> 152102016@smail.iitpkd.ac.in
    
    	ravi@DESKTOP-6VFFD7I:~$ diff b.txt a1.txt > diffba1   ---> compare third with second and write in diffba1
    	ravi@DESKTOP-6VFFD7I:~$ cat diffba1
    	3,4d2
    	< 152102016
    	< 152102016@smail.iitpkd.ac.in

```

2. **[1 mark]** Using an example, show how one can measure the time taken to
   execute a command.

**Ans.**. 
```
	While executing time command, it gives 3 different time ->
        1. Real -> Total time from start to finish of the call. Real time also includes all elapsed time used by other
               processes and time to process spends in blocked state (Interrupt, I/O...)
        2. User -> Amount of CPU time spent in user-mode code i.e. outside the kernel within the process.
                  This is only the actual CPU time used in executing the process.
        3. Sys -> Amount of CPU time spent on system calls in the kernel within the process. 
    
    	ravi@DESKTOP-6VFFD7I:~$ time gcc fac.c -o fac
    	real    0m0.239s
    	user    0m0.037s
    	sys     0m0.048s
    
    	ravi@DESKTOP-6VFFD7I:~$ time ./fac
    	Enter a number: 6
    	Factorial of 6 is: 720
    	real    0m2.482s
    	user    0m0.003s
    	sys     0m0.000s
    
    	ravi@DESKTOP-6VFFD7I:~$ time ls
    	152102016  cat_cut             csv.rollnos.unique  diffba   gem5       hello.in             microbench
    	a.csv      csv.merged          diffa1b             diffba1  hello      hello.out            my_pipe
    	a1.csv     csv.rollnos         diffaa1             fac      hello.c    lab6_soft_hard_link  riscv-gnu-toolchain
    	b.csv      csv.rollnos.sorted  diffab              fac.c    hello.err  ls.in                scs

    	real    0m0.020s
    	user    0m0.002s
    	sys     0m0.006s

```
    
3. **[1 mark]** What is the difference between a hard-link and a soft-link in
   Linux Filesystem. With an example, show the commands you will use to create
   such links.

**Ans.**. 
```
	A symbolic or softlink is an actual link to the original file while hard link is a mirror copy of the original file. If you delete original file, the soft link has no value, because it points to a non-exist file(original file). But in the case of hard link, it can still ha the dta of the original file becasue it acts as a mirror copy of the original link.
    
	=> Different properties of soft link (I mention few of them) -> 
        1. link between directories
        2. has different inodes number and file permissions than original file
        3. permission will not be updated as we change the permissions of original file
        4. has only the path of the original file, not the contents.
        
	=> Different properties of hard link (I mention few of them) ->
        1. can't link directories
        2. has the same inodes number and permissions of original file
        3. permission will be updated as we change the permissions of original file
        4. has the actual contents of original file, even remove or moved original file still you can view the 
        contents of hard link file.
        
	=> Now i am shown you practical examples -->
    	
    	# Soft link 
        ravi@DESKTOP-6VFFD7I:~$ mkdir lab6_soft_hard_link
    	ravi@DESKTOP-6VFFD7I:~$ cd lab6_soft_hard_link
    
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ echo "My name is Ravi Kumar Choubey" > original.file
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ cat original.file
    	My name is Ravi Kumar Choubey
    
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ ln -s original.file softlink.file
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ cat softlink.file
    	My name is Ravi Kumar Choubey
    
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ ls -lia
    	total 12
    	107409 drwxr-xr-x  2 ravi ravi 4096 Oct 25 08:00 .
      	1504 drwxr-xr-x 11 ravi ravi 4096 Oct 25 07:57 ..
    	107410 -rw-r--r--  1 ravi ravi   30 Oct 25 07:59 original.file
    	107411 lrwxrwxrwx  1 ravi ravi   13 Oct 25 08:00 softlink.file -> original.file
    
    	Note: - See here inode number(107410 vs 107411) and also permission (-rw-r--r-- vs lrwxrwxrwx) are different.
    
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ rm original.file
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ cat softlink.file   --> remove original file, also remove softlink.file becuase it is just a link that points to the original file.
    	cat: softlink.file: No such file or directory                   --> For understanding softlink is like a shortcut to a file. If you remove the file, the shortcut is useless.
    
    	# Hard link
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ echo "My name is Ravi Kumar Choubey" > original.file
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ cat original.file
    	My name is Ravi Kumar Choubey
    
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ ln original.file hardlink.file
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ cat hardlink.file
    	My name is Ravi Kumar Choubey
    
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ ls -lia
    	total 16
    	107409 drwxr-xr-x  2 ravi ravi 4096 Oct 25 08:10 .
      	1504 drwxr-xr-x 11 ravi ravi 4096 Oct 25 07:57 ..
    	107410 -rw-r--r--  2 ravi ravi   30 Oct 25 08:09 hardlink.file
    	107410 -rw-r--r--  2 ravi ravi   30 Oct 25 08:09 original.file
    
    	Note: - See here inode number(107410) and also permission (-rw-r--r--) are same.

    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ rm original.file
    	ravi@DESKTOP-6VFFD7I:~/lab6_soft_hard_link$ cat hardlink.file   --> remove original file, still content present of hard link file.
    	My name is Ravi Kumar Choubey

```
    
4. **[1 mark]** How will you create named pipes in UNIX. Show an example usage
   of such named pipes.

**Ans.**. 
```
	Named pipe is simply a advanced version of traditional(unnamed) Unix pipe. Pipe is a way of transferring data from one process to another or one operation to another. By using this Unix pipe, we reduce the operations to access the disk to read/write data into it. 
    
    	# Unnamed Pipe
    	ravi@DESKTOP-6VFFD7I:~$ cat a.csv b.csv | grep 121801050
    	121801050,EE3010,34
    	121801050,EE2140,77
    	121801050,EE3010,34
    	121801050,EE2140,77
    
    	Named Pipe
    
    	# Create a named pipe
    	ravi@DESKTOP-6VFFD7I:~$ mkfifo my_pipe    
    
    	ravi@DESKTOP-6VFFD7I:~$ ls -l my_pipe
    	prw-r--r-- 1 ravi ravi 0 Oct 25 08:48 my_pipe            # 'p' indicates a file of type 'pipe'
    	ravi@DESKTOP-6VFFD7I:~$ file my_pipe
    	my_pipe: fifo (named pipe)

    	ravi@DESKTOP-6VFFD7I:~$ ls
    	152102016  cat_cut             csv.rollnos.unique  diffba   hello.c    lab6_soft_hard_link  riscv-gnu-toolchain
    	a.csv      csv.merged          diffa1b             diffba1  hello.err  ls.in                scs
    	a1.csv     csv.rollnos         diffaa1             gem5     hello.in   microbench
    	b.csv      csv.rollnos.sorted  diffab              hello    hello.out  my_pipe
    
    	ravi@DESKTOP-6VFFD7I:~$ cat a.csv b.csv > my_pipe   # Screen of characters is not sending to disk but send into the pipe_named            
    	ravi@DESKTOP-6VFFD7I:~$                             # Writing to the pipe finishes when all the data is read
    
    	# In other terminal
    	ravi@DESKTOP-6VFFD7I:~$ grep 121801050 < my_pipe   # Read the contents of the pipe. 
    	121801050,EE3010,34
    	121801050,EE2140,77
    	121801050,EE3010,34
    	121801050,EE2140,77

```
    
5. Analysis of the `syslog` file.

    a. **[2 marks]** Count the number of error messages in the `syslog` file.

    **Ans.**. 
```
	ravi@ravi:~/152102016/lab6/echoes$ cat /var/log/syslog | grep 'error' | wc -l
	78

```
 
    b. **[2 marks]** Count the number of lines in the log file. 

    **Ans.**. 
```
	ravi@ravi:~/152102016/lab6/echoes$ cat /var/log/syslog | wc -l
	30272

```

    c. **[2 marks]** Count the number of unique words in the log file.

    **Ans.**. 
```	
	ravi@ravi:~/152102016/lab6/echoes$ cat /var/log/syslog | sort | uniq | wc -w
	369886

```

    d. **[2 marks]** Find the latest error message

    **Ans.**. 
```	
	ravi@ravi:~$ cat /var/log/syslog | grep 'error' | tail -2
	Sep 15 16:40:09 ravi /usr/lib/gdm3/gdm-x-session[1416]: #011(WW) warning, (EE) error, (NI) not implemented, (??) unknown.
	Binary file (standard input) matches

```
		            
    e. **[2 marks]** Find the oldest error message

    **Ans.**. 
```	
	ravi@ravi:~/152102016/lab6/echoes$ cat /var/log/syslog | grep 'error' | head -1
	Sep 11 01:44:14 ravi thermald[634]: [WARN]error: could not parse file /etc/thermald/thermal-conf.xml

```
	    
    f. **[2 marks]** Find the average time between the errors

    **Ans.**. 
```
	n=`grep error /var/log/syslog | wc -l`

	# grep error /var/log/syslog | head -1    # sep 11 01:44:14


	# grep error /var/log/syslog | tail -2    # sep 15  16:40:09

	# difference sep 15  16:40:09 and sep 11 01:44:14 is 4 days, 14 hours, 55 minutes and 55 seconds => 399,355 seconds

	time=399355

	average_time=`expr $time / $n`

	echo "Average time betweeen two errors :- $average_time seconds"
	
	O/p :- Average time betweeen two errors :- 5119 seconds

```

6. `echoes` using pipes. You should do all the following questions inside the
   folder `lab2/echoes`

    a. **[2 marks]** Write a C program (`iecho.c`) that reads an input (using
      `scanf()`) and stores it to a variable named `num`, increments `num` by
      one, and prints the incremented value (using `printf()`). Compile this
      program to create an executable named `iecho`.

    **Ans.**. (Do this in the folder `echoes`. YOU SHOULD NOT COMMIT THE
      	   EXECUTABLE `iecho` INTO YOUR REPO. DOING SO WILL ATTRACT PENALTY).

    b. **[2 marks]** Compile `driver.c` into an executable named `driver`. Pipe
      the output of `driver` as the input of `iecho`.

    **Ans.**.
```
	ravi@ravi:~/152102016/lab6/echoes$ gcc driver.c -o driver
	ravi@ravi:~/152102016/lab6/echoes$ gcc iecho.c -o iecho
	ravi@ravi:~/152102016/lab6/echoes$ ./driver | ./iecho
	Incremented value: 1
	Incremented value: 2
	Incremented value: 3
	Incremented value: 4
	Incremented value: 5

```
        
      
    c. **[8 marks]** Uncomment the lines 9 and 10 of `driver.c`, and recompile
      the code (executable name can continue to be `driver`). Write a command or
      shell script such that `driver` gets (or reads) the incremented value
      (which is nothing but the output of `iecho`). NOTE: This requires a
      two-way communication where `driver` sends a number to `iecho`, `iecho`
      increments it and sends the incremented value back to `driver`. The
      `driver` then prints both the original value sent and the incremented
      value it received on `stderr`. You may change `iecho.c` that you
      implemented for the first sub-part if you wish to, but ensure that the
      earlier implementation of `iecho.c` was committed (and pushed) to the repo
      at least once before editing.

    **Ans.**. 
```
	ravi@ravi:~/152102016/lab6/echoes$ mkfifo driv_named_pipe
	ravi@ravi:~/152102016/lab6/echoes$ mkfifo iecho_named_pipe
	ravi@ravi:~/152102016/lab6/echoes$ ./driver > driv_named_pipe < iecho_named_pipe | ./iecho < driv_named_pipe > iecho_named_pipe
	Even: sent: 0, got 1
	Even: sent: 1, got 2
	Even: sent: 2, got 3
	Even: sent: 3, got 4
	Even: sent: 4, got 5

```
7. **[12 marks]** The folder `lab2/dedup/inputs` has some files that contain
   information about the marks secured by some students in a few subjects. Since
   a lot of such files may be generated in future, you wish to save disk space.
   Write a shell script `dedup.sh` that takes a directory name as an input
   argument, finds all regular files immediately under this directory that are
   duplicates, and replaces the duplicates with soft-links. NOTE: a. The
   filename of the duplicates should remain same (before and after replacing
   them with soft links). b. Your script should not search for duplicates
   recursively. b. The original files that is the one that is lexicographically
   first. c. If there are more than one duplicate, the duplicates should
   continue to link to the original (lexicographically first) file only. d. If
   the script is not given any input argument, it is assume the current working
   directory as its input. e. The filenames can contain special characters such
   as "*", spaces, "~", and leading "-". f. Your script should ignore all
   non-regular files such as links, pipes, directories

    **Ans.**. (Complete this exercise in `dedup.sh` present under `lab2/dedup`)

