import matplotlib.animation as animation
import numpy as np
import matplotlib.pyplot as plt


fig, ax = plt.subplots()
bins = np.arange(-7, 21, 1)


def animate(i):
    normal_data = np.random.normal(2, 2, 100 * i)
    plot_distplot(ax, normal_data, 'Standard normal Distribution', 'n = {}'.format(10000 * i))


def plot_distplot(ax, data, title, annotation):  # use distribution function to plot
    ax.cla()
    ax.hist(data, bins=bins)
    ax.set_title(title)
    ax.set_ylabel('Density')
    ax.set_xlabel('Value')
    ax.annotate(annotation, [3, 27])


ani = animation.FuncAnimation(fig, animate, frames=20, interval=200)

plt.show()
