import matplotlib.pyplot as plt


class Error0(Exception):  # inherit all exceptions inside Error class and pass is used to do nothing inside it
    pass


class Error1(Exception):
    pass


class Error2(Exception):
    pass


class Error3(Exception):
    pass


class Error4(Exception):
    pass


# **************************************************************************************************

class PieChart:
    def __init__(self, my_dict):  # constructor inside contain my dictionary !
        self.my_dict = my_dict

        try:  # check dictionary 'keys and values' are valid or not !
            for key in self.my_dict:

                if type(key) != str:
                    raise Error0
                elif type(self.my_dict[key]) != int or self.my_dict[key] <= 0:
                    raise Error1
                else:
                    pass
        except Error0:
            print("Label should be string")
        except Error1:
            print("Value should be a positive numeric")

    # **********************************************************************************************

    def __add__(self, other):  # used for add operator overloading

        if type(other) is tuple:  # check it is tuple

            length_tuple = len(other)  # length of tuple

            try:  # check tuple is valid or not !

                if not other:  # tuple is empty....
                    raise Error2

                elif type(other[0]) != str:  # first element of tuple is not string...
                    raise Error3

                elif type(other[1]) != int or other[1] <= 0:  # check second element not int or <= 0
                    raise Error4

                # ********************************************************************************************

                elif length_tuple == 2:  # check length is only 2 so, execute !

                    list = [other]  # tuple inside list !

                    x = dict(list)  # typecast list in dictionary because tuple is hashable
                    # and list , dictionary are not hashable...

                    y = self.my_dict

                    for key in set(x):  # used to check keys are same then values should be added !
                        z = {key: x.get(key, 0) + y.get(key, 0)} or {key: x.get(key, 0)}
                        break

                    self.my_dict = self.my_dict | z  # merge two dictionary, proper operator overloading is used here !

                    return self

                else:  # length is > 2
                    print("tuple does not have 2 elements")



            except Error2:
                print("tuple is empty")
                return self

            except Error3:
                print("first tuple element is not a string")
                return self

            except Error4:
                print("second tuple element is not a positive numeric")
                return self

        else:  # dictionary as object pass

            x = other.my_dict

            y = self.my_dict
            z = {}  # create empty dictionary
            for key in set(x) | set(y):  # used to check keys are same then values should be added !
                z.update({key: x.get(key, 0) + y.get(key, 0)})

            # print(z)

            self.my_dict = self.my_dict | z  # used for merging python 3.9+,   # proper operator overloading is used

            # self.my_dict.update(z)
            # print(self.my_dict)

            return self

    # *************************************************************************************************************

    def __sub__(self, other):  # for remove key using sub operator overloading
        if other in self.my_dict:  # key is present
            self.my_dict.pop(other)
        else:
            pass

        return self

    # *******************************************************************************************************

    def show(self):  # create our 'show' method that contain plotting methods of Matplotlib

        activities = self.my_dict.keys()
        sizes = self.my_dict.values()
        # cols = ['b', 'm', 'y', 'c']

        plt.pie(sizes, labels=activities, startangle=90, autopct='%1.1f%%')

        plt.title("Pie Plot")
        # plt.tight_layout()
        # plt.legend(title="activities", loc ="upper right")
        plt.show()


# *********************************************************************************************************

# if you want to check invalid keys and values, kindly First comment out 'p.show()' call...
p = PieChart({'Frogs': 10, 'Dog': 20, 'Cat': 30})
# p = p + ('Cat', 25)
# p = p + ('Dog', 25)
# p = p + PieChart({'Frogs': 20, 'Cat': 10})
p = p - 'Frogs'
p = p - 'Lions'

p.show()
