import matplotlib.pyplot as plt

import numpy as np

pi = np.pi
theta = np.linspace(0, 2 * pi)
plt.figure(1)


class Sines:

    def addSine(self, degree):

        if degree == 0:
            self.degree = np.sin(theta)
            plt.subplot(211)

            plt.plot(theta, self.degree, 'b', label="ϕ=0", linewidth=1)
            plt.grid(color='k')

        elif degree == 90:
            self.degree = np.sin(theta + pi / 2)
            plt.subplot(211)

            plt.plot(theta, self.degree, 'r', label="ϕ=90", linewidth=1)
            plt.grid(color='k')

        else:
            pass

    def shiftRight(self, degree):
        if degree == 45:
            self.degree = np.sin(theta - pi / 4)
            plt.subplot(211)

            plt.plot(theta, self.degree, 'c', linewidth=1)
            plt.grid(color='k')

            self.degree = np.sin(theta + pi / 4)
            plt.subplot(211)

            plt.plot(theta, self.degree, 'g', linewidth=1)
            plt.grid(color='k')

        else:
            pass

    def shiftLeft(self, degree):
        if degree == 45:
            self.degree = np.sin(theta)
            plt.subplot(211)

            plt.plot(theta, self.degree, 'b', linewidth=1)
            plt.grid(color='k')

            self.degree = np.sin(theta + pi / 2)
            plt.subplot(211)

            plt.plot(theta, self.degree, 'r', linewidth=1)
            plt.grid(color='k')

        else:
            pass

    def show(self):

        plt.title("Interactive sinusoidal functions")
        plt.xlabel("")
        plt.ylabel("sin( + ϕ)")
        # plt.legend()
        plt.legend(loc="upper right")
        # plt.grid()

        plt.show()


s = Sines()
s.addSine(0)
s.addSine(90)
s.shiftRight(45)
s.shiftLeft(45)
s.show()

