

import turtle

root = turtle.Screen()
root.title("Paddle Program")
root.bgcolor("brown")
root.setup(width=800, height=600)
root.tracer(0)  # it actually stops the window for updating automatically... we are manually update


# why it can do? -> speedup games little bit...

# def dragging_paddle(x, y):
#     paddle.ondrag(None)
#     x = paddle.xcor()  # i want to know current x coordinates of paddle...
#     x += 20  # add 20 pixels to the x coordinate...
#     paddle.setx(x)  # set x to the new x coordinate...
#     paddle.goto(x, y)
#     paddle.ondrag(dragging_paddle)

def dragging_paddle(x, y):
    paddle.ondrag(None)
    # paddle.setheading(paddle.towards(x, y))
    paddle.penup()  # don't want a line when pen moves

    # paddle.forward(x + 20)
    paddle.goto(x, y)
    # paddle.write(paddle.position())
    paddle.ondrag(dragging_paddle)


def dragging_ball(x, y):
    ball.ondrag(None)
    ball.setheading(ball.towards(x, y))
    ball.penup()  # don't want a line when pen moves

    # ball.goto(x, y)

    ball.goto(x, y)

    ball.ondrag(dragging_ball)


# Paddle     --> create paddle
paddle = turtle.Turtle()  # capital T that is my class name
paddle.speed(0)  # this is not speed of paddle move to Screen, this is speed for turtle module for animation
# we set the speed max possible speed otherwise things goes very slow...
paddle.shape("square")
paddle.color("black")
paddle.shapesize(stretch_wid=1, stretch_len=6)
paddle.penup()  # turtle what they do draw lines while moving paddle but we don't need that line...
paddle.goto(-300, -150)

# paddle.ondrag(dragging_paddle)


# Ball  ---> create ball
ball = turtle.Turtle()  # capital T that is my class name
ball.speed(0)  # this is not speed of paddle move to Screen, this is speed for turtle module for animation
# we set the speed max possible speed otherwise things goes very slow...
ball.shape("square")
ball.color("black")
ball.shapesize(stretch_wid=2, stretch_len=2)
ball.penup()  # turtle what they do draw lines while moving paddle but we don't need that line...
ball.ondrag(dragging_ball)
# ball.goto(0, 0)


# use to write String in Output
pen = turtle.Turtle()
pen.speed(0)  # animation speed
pen.color("white")
pen.penup()  # don't want a line when pen moves
pen.hideturtle()  # hide the turtle (pen don't want to see, see only text)
pen.goto(0, 280)

pen.write("* Move paddle ->  left, right arrows", align='center', font=('Courier', 12, 'normal'))
# pen.goto(0, 260)
# pen.write("* Move ball ->  Up,down,left, right arrows", align='center', font=('Courier', 12, 'normal'))


# move down automatically and falling under gravity of ball


# ball.dx = 2  # separate the ball movement into 2 parts... x movement and y movement
ball.dy = 0.1  # every time ball moves with 2 pixels, like if x is +ve move right 2 and y is +ve move Up 2...


# it is used to down the ball automatically...

# Functions --> moving paddle
def paddle_right():
    x = paddle.xcor()  # i want to know current x coordinates of paddle...
    x += 20  # add 20 pixels to the x coordinate...
    paddle.setx(x)  # set x to the new x coordinate...


def paddle_left():
    x = paddle.xcor()  # i want to know current x coordinates of paddle...
    x -= 20  # subtract 20 pixels to the x coordinate...
    paddle.setx(x)  # set x to the new x coordinate...

'''
# Functions --> moving ball
def ball_right():
    x = ball.xcor()  # i want to know current x coordinates of paddle...
    x += 20  # add 20 pixels to the x coordinate...
    ball.setx(x)  # set x to the new x coordinate...


def ball_left():
    x = ball.xcor()  # i want to know current x coordinates of paddle...
    x -= 20  # add 20 pixels to the y coordinate...
    ball.setx(x)  # set x to the new x coordinate...


def ball_up():
    y = ball.ycor()  # i want to know current x coordinates of paddle...
    y += 20  # add 20 pixels to the x coordinate...
    ball.sety(y)  # set x to the new x coordinate...


def ball_down():
    y = ball.ycor()  # i want to know current x coordinates of paddle...
    y -= 20  # add 20 pixels to the y coordinate...
    ball.sety(y)  # set x to the new x coordinate...

'''
# Keyboard binding
root.listen()  # listen keyboard input
root.onkeypress(paddle_right, 'Right')
root.onkeypress(paddle_left, 'Left')
# root.onkeypress(ball_left, 'Left')
# root.onkeypress(ball_right, 'Right')
# root.onkeypress(ball_up, 'Up')
# root.onkeypress(ball_down, 'Down')

# Main game loop
while True:
    root.update()  # everytime loop runs, it updates the screen...

    # Move the ball
    # ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() - ball.dy)

    # Border checking
    if ball.ycor() < -270:  # ball goes down
        ball.sety(-270)  # stop at bottom position
        # ball.dy *= -1  moving ball up
    if ball.ycor() > 275:
        ball.sety(275)
    if ball.xcor() > 370:
        ball.setx(370)
    if ball.xcor() < -375:
        ball.setx(-375)
    if paddle.xcor() < -340:
        paddle.setx(-340)
    if paddle.xcor() > 330:
        paddle.setx(330)

# if ball.ycor() < -120:  # and ball.ycor() > -120.1:
#      if ball.xcor() == paddle.xcor() and ball.ycor() == paddle.ycor():  # or ball.ycor() < paddle.xcor():
#          ball.sety(-120)

# paddle and ball collisions
# if (ball.ycor() < -120 and ball.ycor() > -120.1) and (ball.ycor() < paddle.xcor() or
#         ball.ycor() > paddle.xcor()):
#     # 1st condition is for touching
#     # to paddle (edges), 2nd for touch at top of paddle and 3rd for touch bottom of paddle...
#
#     ball.sety(-120)
