import time
from tkinter import *
from tkinter.ttk import Progressbar

root = Tk()  # window create


def Start_Progress_Bar():  # contains how the files be executed... define progress bar here
    tasks = int(hello_text.get())
    bar['value'] = 0  # restart from starting progress bar...
    x = 0
    while x < tasks:
        time.sleep(1)  # wait for 1 sec
        bar['value'] += (100 / tasks)
        x += 1
        percent.set("Current Progress :" + str((x / tasks) * 100) + "%")
        text.set(str(x) + "/" + str(tasks) + " Files completed")

        root.update_idletasks()  # after each iteration of while loop, it is going to update window
    percent.set("Download Complete")


percent = StringVar()
text = StringVar()

root.title("Progress Bar")  # title of window
root.geometry("300x300")  # specify  size of window width*height

bar = Progressbar(root, orient=HORIZONTAL, length=200)  # call progress bar method of length 300
bar.place(relx=0.2, rely=0.4)  # given specific location

percentLabel = Label(root, textvariable=percent).place(relx=0.3, rely=0.5)  # String display and given location
textLabel = Label(root, textvariable=text).place(relx=0.4, rely=0.6)  # specify location...

hello = Label(root, text="No. of Files ->", fg="Red",
              bg="Pink")  # string display in this root window, 'fg' change colour
# of string, 'bg' change background colour of string.... also you put hexa code of colour -> #564B9F  search in internet

# hello.grid()  # to show label in window..... grid()/ pack()/ place(specify location)
hello.place(relx=0.1, rely=0.7)  # 0.5 * width_size x-axis and 0.5 * height_size y-axis

hello_button = Button(root, text="Start",
                      command=lambda: Start_Progress_Bar())  # command=Start_Progress_Bar it also works but
# for given some argument in function we need to define lambda

hello_button.place(relx=0.8, rely=0.7)
# hello_button.grid()

hello_text = Entry(root, bd=2)  # create text

hello_text.place(relx=0.4, rely=0.7)  # specify location of text
# hello_text.grid()

root.mainloop()  # implement GUI create a loop
