class Error(Exception):
    pass


class Error1(Exception):
    pass


class Polygon(object):

    def __init__(self, numSides, area):
        self.numSides = numSides
        self.area = area

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c


class Triangle(Polygon):

    def __init__(self, a, b, c):   # without attribute created, we inherit from base class So, i use 'Super' here!
        super().__init__(a, b, c)

    def __repr__(self):  # used to print string rather than print address by object

        if self.a <= 0 or self.b <= 0 or self.c <= 0 or (self.a + self.b <= self.c) or (self.a + self.c <= self.b) or (
                self.b + self.c <= self.a):  # anything true, so no print empty string used
            return ""

        else:    # calculate area
            s = (self.a + self.b + self.c) * 0.5

            x = (s * (s - self.a) * (s - self.b) * (s - self.c)) ** 0.5

            return "Triangle with area %s" % x


t = Triangle(3, 4, 5)
print(t)  # __repr__ used for this

if t.a <= 0 or t.b <= 0 or t.c <= 0:
    try:

        raise Error

    except Error:
        print("Triangle should have positive side-lengths")
else:

    try:
        if (t.a + t.b <= t.c) or (t.a + t.c <= t.b) or (t.b + t.c <= t.a):   # triangle equality used here!
            raise Error1

    except Error1:
        print("Side-lengths do not form a triangle")
