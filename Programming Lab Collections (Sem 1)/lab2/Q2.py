class Error(Exception):   # inherit all exceptions inside Error class and pass used to nothing be done here!
    pass


class Error1(Exception):
    pass


class Polygon:

    def __init__(self, numSides, area):

        self.numSides = numSides
        self.area = area

    def __repr__(self):  # used to print string rather than print address by object
        if self.numSides <= 3 or self.area <= 0:
            return ""   # if any thing true so, empty string print means nothing print
        else:

            return "Polygon with %s sides and area %s" % (self.numSides, self.area)  # Polygon with 13 sides and area
            # 123


p = Polygon(13, 123)  # object 'p' can be instantiated using constructor...

print(p)  # __repr__ used for this

try:

    if p.numSides <= 3:
        raise Error
except Error:
    print("Number of sides should be atleast 3")

try:
    if p.area <= 0:
        raise Error1

except Error1:
    print("Polygon should have positive area")
