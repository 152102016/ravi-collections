class Error(Exception):
    pass


class Error1(Exception):
    pass


class Polygon:

    def __init__(self, numSides, area):
        self.numSides = numSides
        self.area = area

    def __repr__(self):
        if self.numSides < 3 or self.area <= 0:
            return ""

        elif self.numSides == 3:
            return "Triangle with area %s" % self.area

        else:

            return "Polygon with %s sides and area %s" % (self.numSides, self.area)


class Triangle(Polygon):

    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

        s = (self.a + self.b + self.c) * 0.5

        x = (s * (s - self.a) * (s - self.b) * (s - self.c)) ** 0.5
        self.area = x

    def __repr__(self):
        if self.a <= 0 or self.b <= 0 or self.c <= 0 or (self.a + self.b <= self.c) or (self.a + self.c <= self.b) or (
                self.b + self.c <= self.a):
            return ""

        else:

            return "Triangle with area %s" % self.area


class Paper:

    def __init__(self, area):
        self.area = area

    def __add__(self, other):    # operator overloading
        self.area = self.area - other.area
        try:
            if self.area <= 0:
                raise Error1

        except Error1:
            print("Paper does not have sufficient free area to fit the polygon")

        return self

    def __repr__(self):  # used to print string rather than print address of object

        return "Paper has free area %s out of 200, and contains:" % self.area

    def merge(self):
        Polygon(10, 300)
        Polygon(3, 30)

    def erase(self):

        ""



pap = Paper(200)
pap = Paper(-1)
try:
    if pap.area <= 0:
        raise Error

except Error:
    print("Paper should have positive area")



pap = Paper(200)
p = Polygon(10, 100)
pap = pap + p
pap = pap + Polygon(5, 45)
print(type(pap).__name__)


pap = Paper(200)
p1 = Polygon(10, 100)
p2 = Polygon(20, 150)
pap = (pap + p1) + p2


pap = Paper(200)
pap = pap + Polygon(10, 100)
pap = pap + Polygon(20, 50)
pap = pap + Triangle(3, 4, 5)
print(pap)
list = [Polygon(10, 100), Polygon(20, 50), Triangle(3, 4, 5)]
for obj in list:
    print(obj)

pap = Paper(200)
pap = pap + Polygon(3, 100)
print(pap)
list1 = [Polygon(3,100)]
for obj1 in list1:
    print(obj1)



pap = Paper(2000)
pap = pap + Polygon(10, 100)
pap = pap + Polygon(20, 50)
pap = pap + Polygon(10, 200)
pap = pap + Polygon(3, 24)
pap = pap + Triangle(3, 4, 5)
print(pap)
list = [Polygon(10, 100), Polygon(10, 200), Polygon(20, 50), Polygon(3, 24), Triangle(3, 4, 5)]
for obj in list:
    print(obj)

pap.merge()
print(pap)
list = [Polygon(10, 300), Polygon(20, 50), Polygon(3, 30)]
for obj in list:
    print(obj)

pap = pap + Polygon(10, 123)
print(pap)


list = [Polygon(10, 300), Polygon(10, 123), Polygon(20, 50), Polygon(3, 30)]

for obj in list:
    print(obj)


pap = Paper(200)
pap = pap + Polygon(10, 100)
pap = pap + Polygon(20, 50)
pap = pap + Triangle(3, 4, 5)
print(pap)


list = [Polygon(10, 100), Polygon(20, 50), Triangle(3, 4, 5)]

for obj in list:
    print(obj)

pap.erase()

list = [Paper(200)]

for obj in list:
    print(obj)








