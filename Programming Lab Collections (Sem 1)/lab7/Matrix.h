#ifndef MATRIX_H
#define MATRIX_H

#include <iostream> 
#include "log.h" 
#include<string.h>  
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define N 100

using namespace std;


class Matrix
{
public:

int no_of_matrix;
int matrix_completed;
string row,column,col,m2,temp;
string matrix,line;
int scaler;
int count;
int A[100][100];
int i,j;
int matrix2[100][100];
int matrix1[N][N];
int matrix_add[100][100];
int matrix_sub[100][100];
int matrix_mul[100][100];
int matrix_trans_x1[100][100];

int counter = 0;
int nor;      	
int noc;	
int nor1;
int noc1;
	

	

	
 
void operation()      
	{

try
{

	while (getline(cin, line))           // scan one by one line using getline
{
    	
        	if (line.length() == 0 || line[0] == '#')
        {
        
        	continue;
        }
        
       

        	check_for_scalars(line);           // call to check scalar value
        	dimension_extraction(line);        // call to check dimensions
       	matrix_formation(line);           // call to check matrix elements form matrcies
    
     }
     
    
 		
 		
}


catch(exception)
{

L_(lerror) << "Unwanted error" << endl;
}

}

void check_for_scalars( string line)
{
	
	if (isdigit(line[0]) && line[1] != ' ' && line[1] != ',')        // isdigit function used to check integer value
	{	
	

	scaler = stoi(line);   // used to  typecasting into int
	
	
	}}
	
void dimension_extraction(string line)
	{
	
	
	if (isdigit(line[0]) && line[1] == ' ')               // check ' '
	{
	
	
	if(counter == 0){
	nor = (int) line[0] - 48;   // typecasting
	noc = (int) line[2] - 48;
	
	no_of_matrix=0;
	}
	
	if(counter == 1){
	nor1 = (int) line[0] - 48;   // typecasting
	noc1 = (int) line[2] - 48;
	
	no_of_matrix=1;
	}
	
	counter = counter + 1;
	
	} }
	
	
	
	
	
void matrix_formation(string line)                       // matric formed
{  

 string matrix_content;

for(int i=0;i<line.length();i++)   
{

if(line[i]==',')                   // check ','
{	

matrix_content=line;
	
}
}

if(matrix_content.length()!=0 && no_of_matrix==1)
{ 

char x1,y1;
int x2,y2;

for(int j=0;j<noc1;j++)
{

x1=matrix_content[j*2];                 // even place numbers are there   1,2,3...
x2=(int)x1-48;                       // typecasting

matrix2[i][j] =x2;                  // one by one elements store in matrix

}

i+=1;
count+=1;

if(count==nor1) { count=0;i=0;no_of_matrix=1;}
}

 
if(matrix_content.length()!=0 && no_of_matrix==0)
{ 

char x1,y1;
int x2,y2;

for(int j=0;j<noc;j++)
{

x1=matrix_content[j*2];
x2=(int)x1-48;

matrix1[i][j] =x2;

}

i+=1;
count+=1;

if(count==nor) { count=0;i=0;no_of_matrix=0;}
}


}





void scalar_matrix1_addition()                       // scalar-matrix addition

{
for (i = 0; i < nor; i++)
    {
       for (j = 0; j < noc; j++)
       {
            cout << matrix1[i][j] + scaler << " ";
            
		}            
        cout << "\n ";
        
    }
    cout << "\n ";
    
    L_(linfo) << "scalar matrix1 addition successful"<<endl; 
    }
    
 
void scalar_matrix1_subtraction()                           // scalar-matrix subtraction

{
for (i = 0; i < nor; i++)
    {
       for (j = 0; j < noc; j++)
       {
            cout << matrix1[i][j] - scaler << " ";
            
		}            
        cout << "\n ";
        
    }
    cout << "\n ";
    
    L_(linfo) << "scalar matrix1 subtraction successful"<<endl; 
    }


void scalar_matrix1_multiplication()                          // scalar-matrix multiplication

{
for (i = 0; i < nor; i++)
    {
       for (j = 0; j < noc; j++)
       {
            cout << matrix1[i][j] * scaler  << " ";
            
		}            
        cout << "\n ";
        
    }
    cout << "\n ";
    
    L_(linfo) << "scalar matrix1 multiplication successful"<<endl; 
    }


    
void scalar_matrix1_division()                    // scalar-matrix division

{
if (scaler == 0){                           // check scalar value is 0 or not ... if '0'  so, divison becomes undefined...

L_(lerror) << "scalar-matrix division is not possible"<<endl;
}

else
{
for (i = 0; i < nor; i++)
    {
       for (j = 0; j < noc; j++)
       {
            cout <<  matrix1[i][j] / scaler << " ";
            
		}            
        cout << "\n ";
        
    }
    cout << "\n ";
    L_(linfo) << "scalar matrix1 division successful"<<endl; 
    }
    }


void matrix_addition(){                          // matrix addition

if (nor == nor1 && noc == noc1){                                       // check no. of rows and columns are equal or not for matrix addition...

for (i=0; i<nor; i++)
        {
            for ( j=0; j<noc; j++)
            {
                matrix_add[i][j] = matrix1[i][j] + matrix2[i][j];
                cout << matrix_add[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        
        L_(linfo) << "Matrix addition successful"<<endl; 
}

else
{
L_(lerror) << "matrix-addition is not possible"<<endl;

}

}


void matrix_subtraction(){                             // matrix subtraction

if (nor == nor1 && noc == noc1){                        // check no. of rows and columns are equal or not for matrix subtaction...

for (i=0; i<nor; i++)
        {
            for ( j=0; j<noc; j++)
            {
                matrix_sub[i][j] = matrix1[i][j] - matrix2[i][j];
                cout << matrix_sub[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        
        L_(linfo) << "Matrix subtraction successful"<<endl; 
}

else
{
L_(lerror) << "matrix-subtraction is not possible"<<endl;

}

}

void matrix_multiplication(){                      // matrix multiplication

if (noc == nor1){                               // check matrix multiplication condition

for (int i = 0; i < nor; i++) {
        for (int j = 0; j < noc; j++) {
            matrix_mul[i][j] = 0;
 
            for (int k = 0; k < nor1; k++) {
                matrix_mul[i][j] += matrix1[i][k] * matrix2[k][j];
            }
 
            cout << matrix_mul[i][j] << " ";
        }
 
        cout << endl;
    }
cout << endl;

L_(linfo) << "Matrix multiplication successful"<<endl; 
}

else
{
L_(lerror) << "matrix-multiplication is not possible"<<endl;

}

}


void matrix_transpose_x1(){                       // transpose of a matrix

for (i=0; i<nor; i++)                
        {
            for ( j=0; j<noc; j++)
            {
                matrix_trans_x1[i][j] = matrix1[j][i];
                cout << matrix_trans_x1[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        
        L_(linfo) << "Transpose of a matrix successful"<<endl; 
}


void determinant_matrix(){                         // compute determinant .........

for (int i=0; i<nor; i++){
for(int j = 0; j<noc; j++){

matrix1[i][j];
}}

cout << determinantOfMatrix(matrix1, nor) << endl;
L_(linfo) << "determinant computation is successful"<<endl; 

}



void subMatrix(int mat[N][N], int temp[N][N], int p, int q, int n) {
   int i = 0, j = 0;
   // filling the sub matrix
   for (int row = 0; row < n; row++) {
      for (int col = 0; col < n; col++) {
         // skipping if the current row or column is not equal to the current
         // element row and column
         if (row != p && col != q) {
            temp[i][j++] = mat[row][col];
            if (j == n - 1) {
               j = 0;
               i++;
            }
         }
      }
   }
}
int determinantOfMatrix(int matrix[N][N], int n) {
   int determinant = 0;
   if (n == 1) {
      return matrix[0][0];
   }
   if (n == 2) {
      return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
   }
   int temp[N][N], sign = 1;
   for (int i = 0; i < n; i++) {
      subMatrix(matrix, temp, 0, i, n);
      determinant += sign * matrix[0][i] * determinantOfMatrix(temp, n - 1);
      sign = -sign;
   }
   return determinant;


}


};


#endif
