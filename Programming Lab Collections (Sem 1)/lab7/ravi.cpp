#include "Matrix.h"

#include <iostream> 
#include "log.h" 
#include<string.h>  
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define N 100

using namespace std;  



int main(int argc, char *argv[])
{

initLogger( "mylogfile.log", ldebug);                   // create mylogfile.log and contain executed case statement info and errors...

Matrix m;                             // create object 'm'
m.operation();                      // call operation fn of call Matrix

int opt;

while ((opt = getopt(argc,argv,":asmdtx::")) != EOF)
{
switch(opt)
{

case 'a':
m.scalar_matrix1_addition();
m.matrix_addition();
break;

case 's':
m.scalar_matrix1_subtraction();
m.matrix_subtraction();
break;

case 'd':
m.scalar_matrix1_division();
break;

case 'm':
m.scalar_matrix1_multiplication();
m.matrix_multiplication();
break;

case 't':
m.matrix_transpose_x1();
break;

case 'x':
m.determinant_matrix();

}


}

return 0;

}
