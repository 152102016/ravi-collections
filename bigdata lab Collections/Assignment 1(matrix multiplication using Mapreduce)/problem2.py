from mrjob.job import MRJob
from mrjob.step import MRStep
import json
class Matrix_Multiplication_with_1mapred(MRJob):	
	def steps(self):
		return[MRStep(mapper=self.mapper_get_column_as_key,reducer=self.reducer_products_and_sum)]
	
	def mapper_get_column_as_key(self,_,line):
		json_array = json.loads(line)
		
		if(json_array[0]=='a'):
			json_array[0]=0
			for j in range(0,5):            # (i,j)
				yield (json_array[1],j),(json_array[0],json_array[2],json_array[3])
				
		if(json_array[0]=='b'):
			json_array[0]=1
			for j in range(0,5):           # (j,k)
				yield (j,json_array[2]),(json_array[0],json_array[1],json_array[3])
				
	def reducer_products_and_sum(self,key,values):
	
		matrix0_rows = [0,0,0,0,0]
		matrix1_columns = [0,0,0,0,0]
	
		final_value = 0
	
		for others in values:
			if(others[0]==0):
				matrix0_rows[others[1]] = others[2]
			if(others[0]==1):
				matrix1_columns[others[1]] = others[2]
				
		for i in range(0,5):
			final_value = final_value + ( matrix0_rows[i] * matrix1_columns[i])
		
		yield key,final_value
		
if __name__=='__main__':
	Matrix_Multiplication_with_1mapred.run()
