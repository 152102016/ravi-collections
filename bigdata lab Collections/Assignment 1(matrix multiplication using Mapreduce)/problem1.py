from mrjob.job import MRJob
from mrjob.step import MRStep
import json
class Matrix_Multiplications_with_2mapred(MRJob):
	def steps(self):
		return[MRStep(mapper=self.mapper_get_column_as_key,reducer=self.reducer_mul_others),MRStep(mapper=self.mapper_i_k_as_key_and_others_as_values,reducer=self.reducer_sum_products)]
	def mapper_get_column_as_key(self,_,line):
		json_array = json.loads(line)
		others=[]                      # append other values 
		if(json_array[0]=='a'):
			json_array[0]=0
			others.append(json_array[0])
			others.append(json_array[1])
			others.append(json_array[3])
			yield json_array[2],others          # return column(matrix 0), others
		if(json_array[0]=='b'):
			json_array[0]=1
			others.append(json_array[0])
			others.append(json_array[2])
			others.append(json_array[3])
			yield json_array[1],others                # return column(matrix 1), others
	def reducer_mul_others(self,j,values):   # key -> j; values -> others
		
		final = []
		matrix0_rows=[0,0,0,0,0]          # ques have 5*5 matrices so, take 5 rows of matrix 0
		matrix1_columns=[0,0,0,0,0]          # take 5 columns of matrix 1 
		
		for others in values:
			if(others[0]==0):
			    matrix0_rows[others[1]] = matrix0_rows[others[1]] + others[2]               # others[2] -> file values
			else:
			    matrix1_columns[others[1]] = matrix1_columns[others[1]] + others[2]
			    
		for i in range(0,len(matrix0_rows)):
			for j in range(0,len(matrix1_columns)):
			    product_values=0
			    
			    coordinates = []
			    coordinates.append(i)
			    coordinates.append(j)
			    
			    coordinates_products=[]
			    coordinates_products.append(coordinates)
			    
			    product_values = matrix0_rows[i] * matrix1_columns[j]
			    
			    coordinates_products.append(product_values)
			    
			    final.append(coordinates_products)          # final -> [[[coordinates], product], [[],__] , [[],__] .....]
		
		for index in final:
			yield index[0],index[1]
			
	def mapper_i_k_as_key_and_others_as_values(self,key,products):
		yield key,products
	def reducer_sum_products(self,key,products):    # same coordinates, get sum of products
		yield key,sum(products)
		
if __name__=='__main__':
	Matrix_Multiplications_with_2mapred.run()
			    
			    
			    
			    
			    
